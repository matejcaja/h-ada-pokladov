#Zadanie

Cieľom zadania bolo pomocou nadefinovaného hľadača pokladov objaviť čo najviac, resp. všetky poklady na hracej ploche. Hracia plocha predstavovala dvojrozmernú mriežku, po ktorej sa bolo možné hýbať v 4 smeroch, štandardne Hore,Dole,Vľavo,Vpravo. Na hľadanie pokladov bol využitý evolučný algoritmus.

Pre bližšie popísanie projektu viď. súbor Dokumentácia.pdf