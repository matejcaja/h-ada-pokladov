import javax.swing.*;
import java.lang.reflect.Array;
import java.util.BitSet;
import java.util.Random;
import java.util.*;

public class GeneticManager implements Comparator<GeneticSubject>{
    JTextArea textArea;
    private final int sizeOfRubbish = 0;

    public GeneticManager(JTextArea textArea){
        this.textArea = textArea;
    }

    public GeneticManager(){

    }

    //print BitSet as String
    public void printState(GeneticSubject subject, String text, boolean system){
        textArea.append(text +"\n");
        if(system)System.out.println(text);
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                textArea.append(subject.getMap()[i][j]+"");
                if(system)System.out.print(subject.getMap()[i][j]);
            }
            if(system)System.out.println();
            textArea.append("\n");
        }

        for(int v = 0; v < subject.getSteps().size(); v++){
            textArea.append(subject.getSteps().get(v) + " ");
            if(system)System.out.print(subject.getSteps().get(v)+" ");
        }
        if(system)System.out.println();
        textArea.append("\n[Info]: treasureCount: "+subject.getTreasureCount()+"\n");
    }


    public ArrayList<BitSet> setRandomInstruction(){
        Random generator = new Random(System.nanoTime());
        ArrayList<BitSet> list = new ArrayList<BitSet>();

        //for every of 64 instructions
        for(int sizeOfBitSet = 0; sizeOfBitSet < 64; sizeOfBitSet++){
            //for random size from 0 to 7 set random 1 or 0 value
            BitSet bitSets = new BitSet(8);
            for(int bits = 0; bits < generator.nextInt(8); bits++){
                    bitSets.set(generator.nextInt(8));
            }
            list.add(bitSets);
        }

        return list;
    }

    public ArrayList<BitSet> cloneArrayList(ArrayList<BitSet> list){
        ArrayList<BitSet> clone = new ArrayList<BitSet>();
        for(int i = 0; i < list.size(); i++){
            clone.add(list.get(i));
        }
        return clone;
    }

    public int[][] createMap(){
        int[][] map = new int[7][7];

        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7;j++){
                map[i][j] = 0;
            }
        }
        map[5][4] = 1;
        map[4][1] = 1;
        map[2][2] = 1;
        map[1][4] = 1;
        map[3][6] = 1;
        map[6][3] = 2;

        return map;
    }

    @Override
    public int compare(GeneticSubject subject1, GeneticSubject subject2) {
        if(subject1.getFitness() < subject2.getFitness()){
            return 1;
        }else if(subject1.getFitness() > subject2.getFitness()){
            return -1;
        }
        return 0;
    }

    public int computeFitness(int treasureCount, int instructionCount){
        if(treasureCount == 0)
            //if no treasure is found, set to number of instructions made
            return instructionCount;
        else
            return ((treasureCount*10000)-instructionCount);
    }

    public GeneticSubject initializeSubject(){
        GeneticSubject subject = new GeneticSubject(createMap());
        subject.setStartInstructions(setRandomInstruction());
        subject.setFinishInstructions(cloneArrayList(subject.getStartInstructions()));

        return subject;
    }

    //perform tournament and return new subjects with new instructions
    public void tournamentSelection(PriorityQueue<GeneticSubject> priorityQueue, ArrayList<GeneticSubject> subjectArrayList){
        Random generator = new Random(System.nanoTime());
        ArrayList<GeneticSubject> temporaryList = new ArrayList<GeneticSubject>();

        //cut off unwanted, if any
        //System.out.println(priorityQueue.size());
        for(int i = priorityQueue.size(); i > 0 ; i--){
            temporaryList.add(priorityQueue.poll());
        }
        priorityQueue.clear();

        //start of tournament
        while(subjectArrayList.size() != 100){

        ArrayList<GeneticSubject> subjectsToClone = new ArrayList<GeneticSubject>();

        GeneticSubject subject1 = new GeneticSubject();
        GeneticSubject subject2 = new GeneticSubject();
        subjectsToClone.add(subject1);
        subjectsToClone.add(subject2);



            int rand = 0;
            ArrayList<BitSet> newInstructions = new ArrayList<BitSet>();
        //perform tournament
        for(int subjects = 0; subjects < 2; subjects++){
            int k = 0;

            while(k < 2)k = generator.nextInt(temporaryList.size());//pick a size of tournament
            for(int i = 0; i < k; i++){
                rand = generator.nextInt(temporaryList.size());

                if(temporaryList.get(rand).getFitness() > subjectsToClone.get(subjects).getFitness()){//take the strongest as a parent
                    subjectsToClone.get(subjects).setFitness(temporaryList.get(rand).getFitness());
                    subjectsToClone.get(subjects).setStartInstructions(temporaryList.get(rand).getStartInstructions());
                }
            }
            if(subjects == 0){
                for(int i = 0; i < 32; i++){
                    if(generator.nextInt(100) < 20){//20% chance to mutate
                        newInstructions.add(mutateInstruction(temporaryList.get(rand).getStartInstructions().get(i)));
                    }else{
                        newInstructions.add(temporaryList.get(rand).getStartInstructions().get(i));
                    }
                }
            }else{
                for(int i = 32; i < 64; i++){
                    if(generator.nextInt(100) < 20){//20% chance to mutate
                        newInstructions.add(mutateInstruction(temporaryList.get(rand).getStartInstructions().get(i)));
                    }else{
                        newInstructions.add(temporaryList.get(rand).getStartInstructions().get(i));
                    }
                }
            }
        }//create new subject
        GeneticSubject newSubject = new GeneticSubject(createMap());
        newSubject.setStartInstructions(newInstructions);
        newSubject.setFinishInstructions(cloneArrayList(newSubject.getStartInstructions()));

        subjectArrayList.add(newSubject);
           // System.out.println("Velkost listu "+subjectArrayList.size());
        }
    }

    public void rouletteSelection(PriorityQueue<GeneticSubject> priorityQueue, ArrayList<GeneticSubject> subjectArrayList){
        ArrayList<GeneticSubject> temporaryList = new ArrayList<GeneticSubject>();
        ArrayList<BitSet> newInstructions = new ArrayList<BitSet>();
        Random generator = new Random(System.nanoTime());
        int fitnessCap = 0;
        int valueToSearch;
        int valueActual = 0;
        int position = 0;

        for(int i = priorityQueue.size(); i > sizeOfRubbish ; i--){//get sum of fitness values
            fitnessCap += priorityQueue.peek().getFitness();
            //System.out.println(priorityQueue.peek().getFitness());
            temporaryList.add(priorityQueue.poll());
        }

        //System.out.println("Velkost capu "+fitnessCap +" "+temporaryList.size());

        while (subjectArrayList.size() != 100) {
            valueToSearch = fitnessCap==0?0:generator.nextInt(fitnessCap);

            for (int subjects = 0; subjects < 2; subjects++) {
                for (int i = 0; i < temporaryList.size(); i++) {
                    valueActual += temporaryList.get(i).getFitness();
                    //System.out.println(valueActual + " " +valueToSearch);
                    if (valueActual > valueToSearch) {
                        position = i;
                        break;
                    }
                }
                if (subjects == 0) {//perform cossover, take parents instructions
                    for (int i = 0; i < 32; i++) {
                        if (generator.nextInt(100) < 20) {//20% chance to mutate
                            newInstructions.add(mutateInstruction(temporaryList.get(position).getStartInstructions().get(i)));//if, then mutate them
                        } else {
                            newInstructions.add(temporaryList.get(position).getStartInstructions().get(i));
                        }
                    }
                } else {
                    for (int i = 32; i < 64; i++) {
                        if (generator.nextInt(100) < 20) {//20% chance to mutate
                            newInstructions.add(mutateInstruction(temporaryList.get(position).getStartInstructions().get(i)));//if, then mutate them
                        } else {
                            newInstructions.add(temporaryList.get(position).getStartInstructions().get(i));
                        }
                    }
                }
            }
            //create new subject
            GeneticSubject newSubject = new GeneticSubject(createMap());
            newSubject.setStartInstructions(newInstructions);
            newSubject.setFinishInstructions(cloneArrayList(newSubject.getStartInstructions()));

            subjectArrayList.add(newSubject);
        }

    }

    //during creation of subject, mutate its genes
    public BitSet mutateInstruction(BitSet instruction){
        Random generator = new Random(System.nanoTime());

        int k = 2; //number of bits to change
        int rand = generator.nextInt(100);
        if(rand < 80){ //80% chance to mutate
            for(int i = 0; i < k; i++){
               instruction.flip(generator.nextInt(8));
            }
        }

        return instruction;
    }

}
