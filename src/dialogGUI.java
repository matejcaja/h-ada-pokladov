import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.PriorityQueue;

public class dialogGUI extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JRadioButton radioButton1;
    private JRadioButton radioButton2;
    private JTextArea textArea;
    private JButton initializeButton;
    private JButton clearButton;
    private JTextField textField1;
    private JRadioButton vypisyRadioButton;
    private GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();
    public PriorityQueue<GeneticSubject> priorityQueue = new PriorityQueue(100, new GeneticManager()); //sort by fitness
    public GeneticManager manager = new GeneticManager(textArea);
    private ArrayList<GeneticSubject> subjectArrayList = new ArrayList<GeneticSubject>();
    public GeneticSubject result = null;

    private final int sizeOfGeneration = 100;
    private final int sizeOfElite = 20;
    private int totalCount = 1;


    public dialogGUI() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        //radioButtons handling
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(radioButton1);
        buttonGroup.add(radioButton2);
        radioButton2.setSelected(true);

        buttonOK.setEnabled(false);
        textField1.setText("1");

        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });

        initializeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                init();
            }
        });
    }

    private void onOK() {

        main:
        for (int a = 0; a < Integer.parseInt(textField1.getText()); a++) {
            totalCount++;
            System.out.println("[Info]: generation n." + (a + 1));
            priorityQueue.clear();

            for (int i = 0; i < sizeOfGeneration; i++) {
                result = geneticAlgorithm.runAlgorithm((radioButton2.isSelected() ? 1 : 0), textArea, subjectArrayList.get(i));
                if (result.isSuccess()) {
                    manager.printState(result, "[Success]: a subject found all treasures:" + result.getTreasureCount(), vypisyRadioButton.isSelected());
                    textArea.append("[Info]: total count: " + totalCount + "\n");
                    buttonOK.setEnabled(false);
                    break main;
                }
                priorityQueue.add(result);
            }
            //if no subject was successful, create elite subjects and restore their values to default
            if(vypisyRadioButton.isSelected()){
                manager.printState(priorityQueue.peek(), "[Info]: best subject ended with map and steps:",false);
                textArea.append("[Info]: total count: " + totalCount + "\n");
            }
            subjectArrayList.clear();
            for (int i = 0; i < sizeOfElite; i++) {
                priorityQueue.peek().setTreasureCount(0);
                priorityQueue.peek().setFitness(0);
                priorityQueue.peek().setInstructionCount(0);
                priorityQueue.peek().setMap(manager.createMap());
                priorityQueue.peek().getSteps().clear();
                subjectArrayList.add(priorityQueue.poll());
            }
            //start selections and mutations of the rest
            selectionAndMutation();
        }
    }

    private void onCancel() {
        dispose();
    }

    private void init(){
        totalCount = 0;
        subjectArrayList.clear();

        for(int i = 0; i < sizeOfGeneration; i++)
            subjectArrayList.add(manager.initializeSubject());

        buttonOK.setEnabled(true);

        textArea.append("[Success]: inicializacia prebehla uspesne\n");
    }

    private void selectionAndMutation(){
        if(radioButton2.isSelected()){
            manager.tournamentSelection(priorityQueue,subjectArrayList);
        }else if(radioButton1.isSelected()){
            manager.rouletteSelection(priorityQueue,subjectArrayList);
        }
    }

    public static void main(String[] args) {
        dialogGUI dialog = new dialogGUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}

