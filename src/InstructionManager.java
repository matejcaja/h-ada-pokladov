import java.util.ArrayList;
import java.util.BitSet;

public class InstructionManager {
    //get BitSet as int number
    public int getIntegerValue(BitSet bitSet, int from, int to){
        int result = 0;

        for(int i = from ; i < to; i++){
            if(bitSet.get(i)){
                result |= (1 << i);
            }
        }

        return result;
    }

    //convert int value into BitSet
    public BitSet convertIntegerToBitSet(int value){
        return BitSet.valueOf(new long[]{value});
    }

    //get actual map of subject and which direction should it go
    /* return values:
    *   -2 - no direction
    *   -1 - out of map
    *   0 - move without treasure
    *   1 - move with treasure
    */
    public int moveInMap(int[][] map, int direction, ArrayList<String> steps){
        int pomocny;

        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if(map[i][j] == 2){
                    if(direction == 0){ //case hore
                        if((i -1) >= 0){
                            //System.out.println("hore");
                            steps.add("H");
                            pomocny = map[i][j];
                            map[i][j] = map[i-1][j];
                            map[i-1][j] = pomocny;
                            if(map[i][j] == 1){
                                map[i][j] = 0;
                                return 1;
                            }else
                                return 0;
                        }else{
                            return -1;
                        }
                    }
                    else if(direction == 1){ //case dole
                        if((i+1) < 7){
                            //System.out.println("dole");
                            steps.add("D");
                            pomocny = map[i][j];

                            map[i][j] = map[i+1][j];
                            map[i+1][j] = pomocny;
                            if(map[i][j] == 1){
                                map[i][j] = 0;
                                return 1;
                            }else
                                return 0;

                        }else{
                            return -1;
                        }
                    }else if(direction == 2){ //case dolava
                        if((j-1) >= 0){
                            //System.out.println("dolava");
                            steps.add("L");
                            pomocny = map[i][j];

                            map[i][j] = map[i][j-1];
                            map[i][j-1] = pomocny;
                            if(map[i][j] == 1){
                                map[i][j] = 0;
                                return 1;
                            }else
                                return 0;

                        }else{
                            return -1;
                        }
                    }else if(direction == 3){ //case doprava
                        if((j+1) < 7){
                            //System.out.println("doprava");
                            steps.add("P");
                            pomocny = map[i][j];

                            map[i][j] = map[i][j+1];
                            map[i][j+1] = pomocny;
                            if(map[i][j] == 1){
                                map[i][j] = 0;
                                return 1;
                            }else
                                return 0;

                        }else{
                            return -1;
                        }
                    }
                }
            }
        }

        return -2;
    }


}
