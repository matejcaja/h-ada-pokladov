import javax.swing.*;
import java.util.ArrayList;
import java.util.BitSet;


public class GeneticAlgorithm {
    public GeneticSubject runAlgorithm(int selection, JTextArea textArea, GeneticSubject subject){ //selection 1 - turnaj, 0 - ruleta
        GeneticManager manager = new GeneticManager(textArea);
        InstructionManager instructionManager = new InstructionManager();

        int counter = 0;
        int pocetInstrukcii = 0;

       // textArea.append("Pred"+subject.getFinishInstructions().size() + " " + subject.getStartInstructions().size()+"\n");

        ArrayList<BitSet> actualList = subject.getFinishInstructions();
        while (counter != 64){
            if(pocetInstrukcii == 500){
               // System.out.println("[Error]: dovrseny limit poctu instrukcii, hodnota: "+pocetInstrukcii);
                break;
            }
            if(subject.getTreasureCount() == 5){
                System.out.println("[Success]: nasiel som vsetky poklady: "+subject.getTreasureCount());
                subject.setSuccess(true);
                return subject;
            }
            //System.out.println(subject.getTreasureCount());
            BitSet instruction = actualList.get(counter);
            int checkBit1 = instruction.get(7) ? 1 : 0;
            int checkBit2 = instruction.get(6) ? 1 : 0;

            if((checkBit1 == 0) && (checkBit2 == 0)){ //increment
                //textArea.append("inkrementacia\n");
                int position = instructionManager.getIntegerValue(instruction,0,6);
                BitSet modifiedInstruction = actualList.get(position);
                int novaHodnota = instructionManager.getIntegerValue(modifiedInstruction,0,8) +1;
                actualList.set(position, instructionManager.convertIntegerToBitSet(novaHodnota));

                counter++;
            }else if((checkBit2 == 1) && (checkBit1 == 0)){ //decrement
                //textArea.append("dekrementacia\n");

                int position = instructionManager.getIntegerValue(instruction,0,6);
                BitSet modifiedInstruction = actualList.get(position);
                int novaHodnota = instructionManager.getIntegerValue(modifiedInstruction,0,8) -1;
                actualList.set(position, instructionManager.convertIntegerToBitSet(novaHodnota));

                counter++;
            }else if((checkBit1 == 1) && (checkBit2 == 0)){ //jump
                //textArea.append("skok\n");
                counter = instructionManager.getIntegerValue(instruction,0,6);;
            }else if((checkBit1 == 1) && (checkBit2 == 1)){ //move
                int returnVal = instructionManager.moveInMap(subject.getMap(), instructionManager.getIntegerValue(instruction,0,2), subject.getSteps());

                if(returnVal < 0){
                    //System.out.println("[Error]: vybocenie z mapy, smer bol: " +instructionManager.getIntegerValue(instruction,0,2) + " navratova hodnota: " +returnVal);
                    /*for(int i = 0; i < 7; i++){
                        for(int j = 0; j < 7; j++){
                            System.out.print(subject.getMap()[i][j]);
                        }
                        System.out.println();
                    }*/

                    break;
                }else if(returnVal == 1){
                    subject.setTreasureCount(subject.getTreasureCount()+1);
                    subject.setInstructionCount(pocetInstrukcii);
                   // System.out.println("[Success]: a subject found a treasure! current value is: "+subject.getTreasureCount());
                }
                //textArea.append("vypis\n");
                counter++;
            }

            pocetInstrukcii++;
        }

        if(subject.getInstructionCount() == 0) subject.setInstructionCount(pocetInstrukcii);
        //System.out.println("Zostavujem z hodnot "+subject.getStartInstructions().size());
        subject.setFitness(manager.computeFitness(subject.getTreasureCount(), subject.getInstructionCount()));
        //System.out.println(subject.getFitness() + " objekt: "+subject);
        //textArea.append("Po"+subject.getFinishInstructions().size() + " " + subject.getStartInstructions().size()+"\n");
        //System.out.println("[Info]: ukladam jedinca");
        return subject;
    }
}
