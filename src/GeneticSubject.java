import java.util.ArrayList;
import java.util.BitSet;

public class GeneticSubject {
    private int fitness = 0;
    private ArrayList<BitSet> startInstructions = new ArrayList<BitSet>();
    private ArrayList<BitSet> finishInstructions = new ArrayList<BitSet>();
    private int[][] map = new int[7][7];
    private int treasureCount = 0;
    private int instructionCount = 0;
    private ArrayList<String> steps = new ArrayList<String>();
    private boolean success = false;

    public GeneticSubject(int[][] map){

        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7;j++){
                this.map[i][j] = map[i][j];
            }
        }
    }

    public GeneticSubject(){

    }


    public ArrayList<BitSet> getStartInstructions() {
        return startInstructions;
    }

    public void setStartInstructions(ArrayList<BitSet> startInstructions) {
        this.startInstructions = startInstructions;
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public ArrayList<BitSet> getFinishInstructions() {
        return finishInstructions;

    }

    public void setFinishInstructions(ArrayList<BitSet> finishInstructions) {
        this.finishInstructions = finishInstructions;
    }

    public int getTreasureCount() {
        return treasureCount;
    }

    public void setTreasureCount(int treasureCount) {
        this.treasureCount = treasureCount;
    }

    public int getInstructionCount() {
        return instructionCount;
    }

    public void setInstructionCount(int instructionCount) {
        this.instructionCount = instructionCount;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<String> steps) {
        this.steps = steps;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public void setMap(int[][] map) {
        this.map = map;
    }

    public int[][] getMap() {
        return map;
    }
}
